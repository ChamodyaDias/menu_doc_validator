# Couch Menu Doc & API Response Validator

### How to use ?

##### install dependencies

```
npm install
```

```
update param in params.json
update merchantIdList.csv

```

##### build

```
npm run build
```

##### run

```
cd build/
node index.js
```

##### validate 

```
check logs in build/logs dir
```


