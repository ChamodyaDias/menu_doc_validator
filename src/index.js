import _ from 'lodash';
import Mocha from 'mocha';

import { createDefaultLogger, createErrorLogger } from './service/loggerService'

const logger = createDefaultLogger();
const loggerError = createErrorLogger();

const PARAMS = require('./params/params.json').mainParam;

const mocha = new Mocha({
  reporter: 'mochawesome',
  reporterOptions: {
    reportFilename: 'ValidationReport',
    reportTitle: PARAMS[0].runValidationForCouch ? "Menu Validation Report" : PARAMS[0].runValidationForAPI ? "API Validation Report" : "Report" 
  }
}) // Instantiate a Mocha instance.

var testsDir = []
if (PARAMS[0].runValidationForCouch) {
  logger.info("Adding couchDocValidation tests");
  testsDir.push('./service/couchDoc/couchDocValidator');
}
if (PARAMS[0].runValidationForAPI){
  logger.info("Adding apiEndPointValidation tests");
  testsDir.push('./service/api/apiEndPointValidator');
}

if(testsDir.length == 0){
  loggerError.error('No tests specified. Please check and enable one of \'runValidationForCouch\' and \'runValidationForAPI\' parameters inside the params.json file');
  process.exit(-1);
}

for (var i = 0; i < testsDir.length; i++) {
  mocha.addFile(testsDir[i])
}

// Run the tests.
mocha.run(function (failures) {
  // exit with non-zero status if there were failures
  console.log("Logs are saved inside the logs folder. Please note that, logs will be truncated when running the validations again hence if you need them later, please backup logs");
  process.exitCode = failures ? -1 : 0
})