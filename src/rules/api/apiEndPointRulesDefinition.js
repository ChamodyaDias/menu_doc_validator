import _ from 'lodash';
import chai from 'chai';
import { expect } from 'chai';
import chaiExclude from 'chai-exclude';

import { createErrorLogger } from '../../service/loggerService';

const AssertionError = require('assert').AssertionError;

chai.use(chaiExclude);

const addContext = require('mochawesome/addContext');

const loggerError = createErrorLogger();

const assertAPIArraySize = (merchantId, actualObjectArray, expectedObjectArray, rule, sectionName, parentObjId) => {
    describe(`checking array size of type : "${rule.type}", section : ${sectionName}`, () => {
        it('array size should be equal', function () {
            addContext(this, {
                title: `merchantId ${merchantId}`,
                value: {
                    section: sectionName,
                    parentObjectId: parentObjId
                }
            })
            try {
                expect(actualObjectArray.length).to.be.equal(expectedObjectArray.length, "Array lengths are equal");
            } catch (error) {
                let diff = [];
                _.differenceBy(actualObjectArray, expectedObjectArray, rule.uniqueKey).forEach(item => {
                    diff.push(!_.isNil(item[rule.uniqueKey]) ? item[rule.uniqueKey] : item);
                });
                if (_.isEmpty(diff)) {
                    _.differenceBy(expectedObjectArray, actualObjectArray, rule.uniqueKey).forEach(item => {
                        diff.push(!_.isNil(item[rule.uniqueKey]) ? item[rule.uniqueKey] : item);
                    });
                }
                throw new AssertionError({
                    'message': `Array sizes not equal. \n Difference : [${diff}], \n difference count : ${diff.length}`,
                    'expected': expectedObjectArray.length,
                    'actual': actualObjectArray.length
                });
            }
        });
    });
}

export const assertAPIObject = (merchantId, actualObject, expectedObject, rules, sectionName, parentObjId = null) => {
    rules.forEach(rule => {
        const actualElements = _.get(actualObject, rule.path, null);
        const expectedElements = _.get(expectedObject, rule.path, null);
        if (!_.isNil(expectedElements) && !_.isNil(actualElements)) {

            if (rule.checkObjectArraySize) {
                assertAPIArraySize(merchantId, actualElements, expectedElements, rule, sectionName, parentObjId);
            }

            if (_.some(expectedElements, _.isObject)) {
                expectedElements.forEach(element => {
                    const expected = element;
                    const actual = actualElements.find(x => x[rule.uniqueKey] === expected[rule.uniqueKey]);

                    let parentObjectId = null;
                    if (!_.isNil(actual)) {
                        parentObjectId = actual[rule.uniqueKey]
                        describe(`checking object of type : "${rule.type}", unique key : "${actual[rule.uniqueKey]}"`, () => {
                            it('objects should be equal', function () {
                                addContext(this, {
                                    title: `merchantId : ${merchantId}, section : ${sectionName}, object key : ${actual[rule.uniqueKey]}`,
                                    value: {
                                        section: sectionName,
                                        ObjectKey: actual[rule.uniqueKey],
                                        parentObjectId: parentObjId
                                    }
                                });
                                expect(actual).excludingEvery(rule.alwaysExcludedProperties).to.deep.equal(expected);
                            });
                        });

                        if (!_.isNil(rule.childRules)) {
                            assertAPIObject(merchantId, actual, expected, rule.childRules, sectionName, parentObjectId);
                        }
                    }
                });

            } else {
                describe(`checking objects of array : "${rule.type}", section : ${sectionName}`, () => {
                    it('objects should be equal', function () {
                        addContext(this, {
                            title: `merchantId : ${merchantId}, section : ${sectionName}`,
                            value: {
                                section: sectionName,
                                parentObjectId: parentObjId
                            }
                        });
                        expect(actualElements).to.have.members(expectedElements);
                    });
                });
            }
        }
        else {
            describe(`merchantId : ${merchantId}, section : ${sectionName}, path : ${rule.path} - test skipped!`, () => {
                it(`Some elements are undefined or null! Please check the error log for more details`, function () {
                    loggerError.error(`merchantId : ${merchantId}, section : ${sectionName} - test skipped! \n 
                    Menucore elements ${_.isEmpty(expectedElements)}, \n
                    Enterprise elements ${actualElements}`);
                    this.skip();
                });
            });
        }
    });
}
