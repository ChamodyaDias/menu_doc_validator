import _ from 'lodash';
import chai from 'chai';
import { expect } from 'chai';
import chaiExclude from 'chai-exclude';

import { createErrorLogger } from '../../service/loggerService';

chai.use(chaiExclude);

const addContext = require('mochawesome/addContext');
const AssertionError = require('assert').AssertionError;

const ATTRIBUTE_SET = "attributeSet";

const loggerError = createErrorLogger();

const assertCouchArraySize = (merchantId, actualObjectArray, expectedObjectArray, rule, sectionName, parentObjId) => {
    describe(`merchant ID : ${merchantId}, checking array size of type : "${rule.type}"`, () => {
        it('array size should be equal', function () {
            addContext(this, {
                title: `merchantId ${merchantId}`,
                value: {
                    section: sectionName,
                    parentObjectId: parentObjId
                }
            });
            try {
                expect(actualObjectArray.length).to.be.equal(expectedObjectArray.length, "Array lengths are equal");
            } catch (error) {
                let diff = [];
                _.differenceBy(actualObjectArray, expectedObjectArray, rule.uniqueKey).forEach(item => {
                    diff.push(item[rule.uniqueKey]);
                });
                if (_.isEmpty(diff)) {
                    _.differenceBy(expectedObjectArray, actualObjectArray, rule.uniqueKey).forEach(item => {
                        diff.push(item[rule.uniqueKey]);
                    });
                }
                throw new AssertionError({
                    'message': `Array sizes not equal. Difference : [${diff}], difference count : ${diff.length}`,
                    'expected': expectedObjectArray.length,
                    'actual': actualObjectArray.length
                });
            }

        });
    });
}

export const assertCouchObject = (merchantId, actualObject, expectedObject, rules, sectionName, parentObjId = null) => {
    rules.forEach(rule => {
        const actualElements = _.get(actualObject, rule.path, null);
        const expectedElements = _.get(expectedObject, rule.path, null);
        if (!_.isNil(expectedElements) && !_.isNil(actualElements)) {

            if (rule.checkObjectArraySize) {
                assertCouchArraySize(merchantId, actualElements, expectedElements, rule, sectionName, parentObjId);
            }

            //Skip checking each and every attribute set since the IDs are different
            if (_.isEqual(ATTRIBUTE_SET, rule.path)) return;

            expectedElements.forEach(element => {
                const expected = element;
                const actual = actualElements.find(x => x[rule.uniqueKey] === expected[rule.uniqueKey]);
                
                let parentObjectId = null;
                if (!_.isNil(actual)) {
                    parentObjectId = actual[rule.uniqueKey]
                    describe(`merchant ID : ${merchantId}, checking object of type : "${rule.type}", unique key : "${actual[rule.uniqueKey]}"`, () => {
                        it('objects should be equal', function () {
                            addContext(this, {
                                title: `merchantId : ${merchantId}, section : ${sectionName}, object key : ${actual[rule.uniqueKey]}`,
                                value: {
                                    section: sectionName,
                                    ObjectKey: actual[rule.uniqueKey],
                                    parentObjectId: parentObjId
                                }
                            });
                            expect(actual).excludingEvery(rule.alwaysExcludedProperties).to.deep.equal(expected);
                        });
                    });
                }

                if (!_.isNil(rule.childRules)) {
                    assertCouchObject(merchantId, actual, expected, rule.childRules, sectionName, parentObjectId);
                }
            });
        }
        else {
            describe(`merchantId : ${merchantId}, section : ${sectionName} - test skipped!`, () => {
                it(`Some elements are undefined or null! Please check the error log for more details`, function () {
                    loggerError.error(`merchantId : ${merchantId}, section : ${sectionName} - test skipped! \n 
                    Expected elements ${expectedElements}, \n
                    Actual elements ${actualElements}`);
                    this.skip();
                });
            });
        }

    });
}
