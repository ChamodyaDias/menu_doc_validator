import request from 'request-promise';
import Axios from 'axios';
import _ from 'lodash';
import { createDefaultLogger, createSuccessLogger, createErrorLogger } from '../loggerService'

const logger = createDefaultLogger();
const loggerSuccess = createSuccessLogger();
const loggerError = createErrorLogger();

const PARAMS = require('../../params/params.json').mainParam;

const URL_CONFIG = require('../../configs/couchConfigs.json');

const MENU_DOC = 'menu';
const VIRTUAL_PRINTERS_DOC = 'virtualPrinters';

const getCouchUrlFromParam = () => {
    logger.info(`Using environment : ${PARAMS[0].environment}`);
    switch (PARAMS[0].environment) {
        case "pos1":
            return URL_CONFIG.pos1;
        case "pos2":
            return URL_CONFIG.pos2;
        case "pos3":
            return URL_CONFIG.pos3;
        case "pos4":
            return URL_CONFIG.pos4;
        case "tst2":
            return URL_CONFIG.tst2;
        case "tst3":
            return URL_CONFIG.tst3;
        default:
            loggerError.error("Invalid Environment. Default case reached! Using POS-2 as the default environment");
            return URL_CONFIG.pos2;
    }

}

const getDocByRev = ({ merchantId, doc, rev }) => {
    const revParam = rev ? `rev=${rev}` : '';

    const host = getCouchUrlFromParam();
    const uri = host + '/' + merchantId + doc + `?${revParam}`;
    logger.info(`calling couch endpoint : ${uri}`);

    let options = {
        uri,
        json: true
    };
    return request(options)
        .then((res) => {
            return res.documentObject;
        })
        .catch((err) => {
            if (err.statusCode == '404') {
                return Promise.reject(new Error('Db not found.'));
            } else {
                return Promise.reject(err);
            }
        });
};

const getCurrentAndPrevDocs = ({ merchantId, doc }) => {
    let docs = [];

    const host = getCouchUrlFromParam();
    const uri = host + '/' + merchantId + doc + '?revs_info=true';
    logger.info(`calling couch endpoint : ${uri}`);

    let options = {
        uri,
        json: true
    };
    return request(options)
        .then((res) => {
            logger.info("current document and revision info received");
            const revisionInfo = res._revs_info;
            docs.push(res.documentObject);
            if (revisionInfo.length > 1) {
                const prevRev = revisionInfo[1];
                if (prevRev.status === 'available') {
                    return prevRev.rev;
                } else {
                    throw Error(`[${merchantId}] Prev revision is missing.`);
                }
            } else {
                throw Error(`[${merchantId}] No previous revision found.`);
            }
        })
        .then(async (prevRevNum) => {
            const prevDoc = await getDocByRev({ merchantId, doc, rev: prevRevNum });
            docs.push(prevDoc);
            return docs;
        })
        .catch((err) => {
            if (err.statusCode == '404') {
                return Promise.reject(new Error('Db not found.'));
            } else {
                return Promise.reject(err);
            }
        });
};

export const getCouchDocuments = async (merchantId, isMenuDoc) => {
    // const currentCouchDoc = require('./pos1-menu.json');
    // const prevCouchDoc = require('./stag-menu.json');
    logger.info(`attempting to get ${isMenuDoc ? MENU_DOC : VIRTUAL_PRINTERS_DOC} documents for merchant id : ${merchantId}`)
    try {
        const res = await getCurrentAndPrevDocs({ merchantId, doc: isMenuDoc ? `/${MENU_DOC}` : `/${VIRTUAL_PRINTERS_DOC}` });
        logger.info(`get ${isMenuDoc ? MENU_DOC : VIRTUAL_PRINTERS_DOC} documents successful`);
        return Promise.resolve({
            currentFile: res[0],
            oldFile: res[1]
        });
    }
    catch (e) {
        loggerError.error(`[${merchantId}] get couch documents failed! error is ${e}`);
        return Promise.reject(e);
    }
};
