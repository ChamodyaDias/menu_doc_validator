import _ from 'lodash';
import fs from 'fs';
import { getCouchDocuments } from './cloudCouchService';

import { createDefaultLogger, createSuccessLogger, createErrorLogger } from '../loggerService';

const RULES_DEF = require('../../rules/couchDoc/couchDocRulesDefinition.js');
const COUCH_DOC_RULES = require('../../rules/couchDoc/couchDocValidationRules').rules;
const SECTIONS = require('../../models/couchDocSections.json').sections

const logger = createDefaultLogger();
const loggerSuccess = createSuccessLogger();
const loggerError = createErrorLogger();

const getSectionFromFile = (file, key) => {
    return _.isEqual(key, "globalModifiers") ? file : file[key];    
}

const getRulesForSection = (section) => {
    switch (section) {
        case "categories":
            return COUCH_DOC_RULES[Object.keys(COUCH_DOC_RULES)[0]];
        case "attributeSets":
            return COUCH_DOC_RULES[Object.keys(COUCH_DOC_RULES)[1]];
        case "taxCategories":
            return COUCH_DOC_RULES[Object.keys(COUCH_DOC_RULES)[2]];
        case "globalModifiers":
            return COUCH_DOC_RULES[Object.keys(COUCH_DOC_RULES)[3]];
        case "session":
            return COUCH_DOC_RULES[Object.keys(COUCH_DOC_RULES)[4]];
        case "printers":
            return COUCH_DOC_RULES[Object.keys(COUCH_DOC_RULES)[5]];
    }
}

const validateMenuDocuments = (merchantId) => {
    return getCouchDocuments(merchantId, true).then(async (res) => {
        await validateDocuments(merchantId, res);
    }).catch(e => {
        loggerError.error(`Error validating menu documents, error is ${e.stack}`);
        return Promise.reject(new Error(e));
    });
}

const validateVirtualPrintersDocument = (merchantId) => {
    return getCouchDocuments(merchantId, false).then(res => {
        logger.info("couch documents received... Processing...");
        let { currentFile, oldFile } = res;
        return new Promise((resolve) => {
            logger.info('Validating printers');
            const actualObject = currentFile;
            const expectedObject = oldFile;
            const rules = getRulesForSection("printers");
            RULES_DEF.assertCouchObject(merchantId, actualObject, expectedObject, rules["printers"], "printers", null);
            resolve({ merchantId });
        });
    }).catch(e => {
        loggerError.error(`Error validating virtual printers, error is ${e}`);
        return Promise.reject(new Error(e));
    });
}

const validateDocuments = (merchantId, documentObject) => {
    let { currentFile, oldFile } = documentObject;
    let promise = Promise.resolve();
    SECTIONS.map(section => {
        const key = Object.keys(section)[0];
        promise = promise.then(() => {
            logger.info(`Validating section: ${key}`);
            const actualObject = getSectionFromFile(currentFile, key);
            const expectedObject = getSectionFromFile(oldFile, key);
            const rules = getRulesForSection(key);
            RULES_DEF.assertCouchObject(merchantId, actualObject, expectedObject, rules[key], key, null);
        });
    });
    loggerSuccess.info(`Couch Menu document validation completed for [${merchantId}]. Please view the \'ValidationReport.html\' inside mochawesome-report folder for results`);
    return promise;
}

const mainFunction = (merchantIds) => {
    let promise = Promise.resolve();
    merchantIds.map(async (id) => {
        promise = promise.then(async () => {
            logger.info(`Starting validation for merchant id : ${id}`);
            await validateMenuDocuments(id).finally(() => {
                return validateVirtualPrintersDocument(id);
            });
        }).catch(e => {
            loggerError.error(`Validation failed, error is ${e}`);
        });
    });
    return promise;
}

describe("couch menu validation", () => {
    logger.info("Running Validation for couch documents");
    logger.info("Reading Merchant ID list");
    var merchantIds = fs.readFileSync('merchantIdList.csv').toString().split("\n");
    if (_.isNull(merchantIds)) {
        return loggerError.error("Cannot read merchant ID list from file");
    }
    it(" ", (done) => {
        mainFunction(merchantIds).then(() => {
            logger.info('Validation completed');
            done();
        });
    }).timeout(1000000);
});
