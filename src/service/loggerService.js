import log4js from 'log4js';

log4js.configure({
    appenders: {
        appLogs: { type: 'file', filename: './logs/app.log', flags : 'w' },
        successLogs: { type: 'file', filename: './logs/success.log', flags : 'w' },
        errorLogs: { type: 'file', filename: './logs/error.log', flags : 'w' }
    },
    categories: {
        success: { appenders: ['successLogs'], level: 'info' },
        error: { appenders: ['errorLogs'], level: 'error' },
        default: { appenders: ['appLogs'], level: 'info' }
    }
});

export const createSuccessLogger = () => {
    return log4js.getLogger('success');
}

export const createErrorLogger = () => {
    return log4js.getLogger('error');
}

export const createDefaultLogger = () => {
    return log4js.getLogger();
}