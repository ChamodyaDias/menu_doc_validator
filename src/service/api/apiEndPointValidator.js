import _ from 'lodash';
import fs from 'fs';
import { getApiEndPointData } from './apiRequestService';

import { createDefaultLogger, createSuccessLogger, createErrorLogger } from '../loggerService';

const RULES_DEF = require('../../rules/api/apiEndPointRulesDefinition.js');
const API_END_POINT_RULES = require('../../rules/api/apiEndPointValidationRules').rules;
const RESPONSE_SECTIONS = require('../../models/apiEndPointSections.json').sections

const logger = createDefaultLogger();
const loggerSuccess = createSuccessLogger();
const loggerError = createErrorLogger();

const getRulesForSection = (section) => {
    switch (section) {
        case "sessions":
            return API_END_POINT_RULES[Object.keys(API_END_POINT_RULES)[0]];
        case "sessionSlots":
            return API_END_POINT_RULES[Object.keys(API_END_POINT_RULES)[1]];
        case "modifierGroups":
            return API_END_POINT_RULES[Object.keys(API_END_POINT_RULES)[2]];
        case "modifierOptions":
            return API_END_POINT_RULES[Object.keys(API_END_POINT_RULES)[3]];
        case "categories":
            return API_END_POINT_RULES[Object.keys(API_END_POINT_RULES)[4]];
        case "items":
            return API_END_POINT_RULES[Object.keys(API_END_POINT_RULES)[5]];
        case "salesCategories":
            return API_END_POINT_RULES[Object.keys(API_END_POINT_RULES)[6]];
        case "taxes":
            return API_END_POINT_RULES[Object.keys(API_END_POINT_RULES)[7]];
        case "printers":
            return API_END_POINT_RULES[Object.keys(API_END_POINT_RULES)[8]];
    }
}

const validateApiEndPoint = (merchantId) => {
    return getApiEndPointData(merchantId, true).then(async (res) => {
        await validateResponses(merchantId, res);
    }).catch(e => {
        loggerError.error(`Error validating api endpoints, error is ${e}`);
        return Promise.reject(new Error(e));
    });
}

const validateResponses = (merchantId, documentObject) => {
    let { menuCoreResponse, enterpriseMenuResponse } = documentObject;
    let promise = Promise.resolve();
    RESPONSE_SECTIONS.map(section => {
        const key = Object.keys(section)[0];
        promise = promise.then(() => {
            logger.info(`Validating section: ${key}`);
            const rules = getRulesForSection(key);
            RULES_DEF.assertAPIObject(merchantId, enterpriseMenuResponse, menuCoreResponse, rules[key], key);
        });
    });
    loggerSuccess.info('API end point validation complete. Please view the \'ValidationReport.html\' inside mochawesome-report folder for results');
    return promise;
}

const mainFunction = (merchantIds) => {
    let promise = Promise.resolve();
    merchantIds.map((id) => {
        promise = promise.then(() => {
            logger.info(`Starting validation for merchant id : ${id}`);
            return validateApiEndPoint(id);
        }).catch(e => {
            loggerError.error(`Validation failed, error is ${e}`);
        });

    });
    return promise;
}

describe("api end point validation", () => {
    logger.info("Running Validation for API Endpoints");
    logger.info("Reading Merchant ID list");
    var merchantIds = fs.readFileSync('merchantIdList.csv').toString().split("\n");
    if (_.isNull(merchantIds)) {
        return loggerError.error("Cannot read merchant ID list from file");
    }
    it(" ", (done) => {
        mainFunction(merchantIds).then(() => {
            logger.info('Validation completed');
            done();
        });
    }).timeout(100000);
});
