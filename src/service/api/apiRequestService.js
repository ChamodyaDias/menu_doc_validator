import Axios from 'axios';
import _ from 'lodash';
import { createDefaultLogger, createSuccessLogger, createErrorLogger } from '../loggerService'

const logger = createDefaultLogger();
const loggerSuccess = createSuccessLogger();
const loggerError = createErrorLogger();

const URL_CONFIG = require('../../configs/apiConfigs.json').endpoints;

const getEndPointUrlFromConfig = () => {
    logger.info('Getting endpoints from configs');
    return {
        hostMC: URL_CONFIG.menucore,
        hostEM: URL_CONFIG.enterprise
    };

}

const callEndPoint = (accountId) => {
    let { hostMC, hostEM } = getEndPointUrlFromConfig();

    const menuCoreEndPoint = hostMC;
    const enterpriseEndPoint = hostEM;

    logger.info(`calling menucore endpoint : ${menuCoreEndPoint} and enterprise endpoint : ${enterpriseEndPoint}`);

    let responses = [];
    return Axios.get(menuCoreEndPoint).then(res => {
        responses.push(res.data);
        loggerSuccess.info('Obtaining menucore endpoint data successfull');

        return Axios.get(enterpriseEndPoint).then(res => {
            loggerSuccess.info('Obtaining enterprise endpoint data successfull');
            responses.push(res.data);
            return responses;

        }).catch(e => {
            return Promise.reject({
                error: e,
                endpoint: enterpriseEndPoint
            });
        });

    }).catch(e => {
        let error_msg = {}
        if (_.isNil(e.endpoint)){
            error_msg.error = e;
            error_msg.endpoint = menuCoreEndPoint;
        }else{
            error_msg = e
        }

        loggerError.error(`Error obtaining data from end point ${error_msg.endpoint}. Error is ${error_msg.error}`);
        return Promise.reject(error_msg.error);
    });
}

const getAccountIdFromMerchantId = (merchantId) => {
    let idSplit = merchantId.split('-')
    return !_.isUndefined(idSplit[1]) ? idSplit[1] : new Error("Merchant ID invalid");
}

export const getApiEndPointData = (merchantId) => {
    let accountId = getAccountIdFromMerchantId(merchantId);
    logger.info(`attempting to get api end point data for account id : ${accountId}`);

    return callEndPoint(accountId).then((res) => {
        loggerSuccess.info(`get end point data successful`);

        return Promise.resolve({
            menuCoreResponse: res[0],
            enterpriseMenuResponse: res[1]
        });

    }).catch(e => {
        return Promise.reject(e);
    });
};
